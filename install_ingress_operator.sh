#!/usr/bin/env bash

kubectl --kubeconfig demo.kubeconfig apply -f https://raw.githubusercontent.com/nginxinc/nginx-ingress-operator/master/deploy/crds/k8s.nginx.org_nginxingresscontrollers_crd.yaml
kubectl --kubeconfig demo.kubeconfig apply -f https://raw.githubusercontent.com/nginxinc/nginx-ingress-operator/master/deploy/service_account.yaml
kubectl --kubeconfig demo.kubeconfig apply -f https://raw.githubusercontent.com/nginxinc/nginx-ingress-operator/master/deploy/role.yaml
kubectl --kubeconfig demo.kubeconfig apply -f https://raw.githubusercontent.com/nginxinc/nginx-ingress-operator/master/deploy/role_binding.yaml
kubectl --kubeconfig demo.kubeconfig apply -f https://raw.githubusercontent.com/nginxinc/nginx-ingress-operator/master/deploy/operator.yaml

